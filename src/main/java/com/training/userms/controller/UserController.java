package com.training.userms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.userms.entity.UserEntity;
import com.training.userms.model.ResponseDto;
import com.training.userms.service.UserService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/users")
@AllArgsConstructor
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	private UserService userService;

	@PostMapping
	public ResponseEntity<ResponseDto> saveUser(@RequestBody UserEntity user) {
		log.info("USER: Creating user: {}", user);
		ResponseDto savedUser = null;
		try{
			savedUser = userService.save(user);
			log.info("USER: Created successful: {}", savedUser);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseDto> getUser(@PathVariable("id") Long userId) {
		log.info("USER: Retrieving user with id: {}", userId);
		ResponseDto responseDto = null;
		try{
			responseDto = userService.getUser(userId);
			log.info("USER: Retrieved successful: {}", responseDto);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return ResponseEntity.ok(responseDto);
	}
}
